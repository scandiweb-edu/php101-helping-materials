#Education Session 1
The repository contains some code and necessary tools packed into the Docker.

#Prerequisites
Please ensure you got set up and working:
- docker CE latest version
OR
- PHPUnit ^6.3
- PHP 7.1 CLI

#Setup
- clone the repository: 
	```git clone git@bitbucket.org:scandiweb-edu/session1.git```
- Get into the working directory:
	```cd session1```
- Build the image
	```docker build . -t session:1.0```
- Run the container
	```docker run --name session-container -it -v $(pwd)/code:/code session:1.0 bash```
	Pay attention to provide the real full path on the left side (MacOS users pay attention to symlinks)
- Get inside the container:
	```docker exec -it session-container bash```


#Description
These are materials for EDU workshop, that are used during the session. Plan and guide for leader is available after the session or on request.