<?php
declare(strict_types=1);
require_once('bootstrap.php');

//Get Calculator instance
$calc = $resolver->get('Calculator');

//Just some calls to see how it works
$raw = $calc->getString('22/2');
echo "22 / 2 = " . $calc->makeItWork($raw) . "\n";
//
$raw = $calc->getString('12*12');
echo "12 * 12 = " . $calc->makeItWork($raw) . "\n";

$raw = $calc->getString("20+3");
echo "20 + 3 = " . $calc->add($raw) . "\n";

$raw = $calc->getString("5-50");
echo "5 - 50 = " . $calc->add($raw) . "\n";

$raw = $calc->cnvrtDec(20);
echo "20(10) = $raw(2) \n";

$raw = $calc->bintodec(10100);
echo "10100(2) = $raw(10) \n";