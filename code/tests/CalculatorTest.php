<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers Calculator
 */
final class CalculatorTest extends TestCase
{
    /**
     * @var Calculator
     */
    private $calc;

    /**
     * CalculatorTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->calc = new Calculator();
    }

    public function testActionAdd(): void
    {
        $calc = $this->calc;
        $this->assertEquals(15, $calc->getString('3+12'));
    }

    public function testActionMultiple(): void
    {
        $calc = $this->calc;
        $this->assertEquals(18, $calc->makeItWork('3*6'));
        $this->assertEquals(144, $calc->makeItWork('12*12'));
    }

    public function testActionSubstract(): void
    {
        $calc = $this->calc;
        $this->assertEquals(31, $calc->makeItWork('40-9'));
        $this->assertEquals(31, $calc->makeItWork('140-109'));
        $this->assertEquals(-3, $calc->makeItWork('5-8'));
    }

    public function testAddition(): void
    {
        $calc = $this->calc;
        $this->assertEquals(10, $calc->add('5+5'));
        $this->assertEquals(12, $calc->add('7+5'));
        $this->assertEquals(180, $calc->add($calc->getString('90+90')));
        $this->assertEquals(2233, $calc->add('2200+33'));
    }

    public function testDivide(): void
    {
        $calc = $this->calc;
        $this->assertEquals(3, $calc->dvd('9/3'));
        $this->assertEquals(100, $calc->dvd('1000/10'));
    }

    public function testMultiple(): void
    {
        $calc = $this->calc;
        $this->assertEquals(12, $calc->mltpl('3*4'));
        $this->assertEquals(30, $calc->mltpl('2*15'));
    }

    public function testParseString(): void
    {
        $parsedArray = $this->calc->getString('151515+1515');
        $this->assertEquals(3, count($parsedArray));
        $this->assertEquals(151515, $parsedArray[0]);
        $this->assertEquals('+', $parsedArray[1]);
        $this->assertEquals(1515, $parsedArray[2]);
    }

    public function testSubstraction(): void
    {
        $calc = $this->calc;
        $this->assertEquals(3, $calc->sbstrct('3-3'));
        $this->assertEquals(255, $calc->sbstrct('300-45'));
    }
}