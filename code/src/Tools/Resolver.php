<?php

namespace Tools;


class Resolver
{
    private $registry = [];
    
    public function get(string $key)
    {
        if (!array_key_exists($key, $this->registry)) {
            return null;
        }
        
        return new $this->registry[$key];
    }
    
    public function register($abstraction, $implementation)
    {
        if (array_key_exists($abstraction, $this->registry)) {
            return;
        }
        
        return $this->registry[$abstraction] = $implementation;
    }
}