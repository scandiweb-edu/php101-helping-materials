<?php

class Calculator
{
    private $system;
    private $cv;
    
    public function __construct()
    {
        $this->system = 2;
    }

    public function add($input)
    {
        return $input[0] + $input[2];
    }

    public function addString($string)
    {
        $this->string = $string;
    }

    public function cnvrtDec($decimal)
    {
        $current = $decimal;
        $res = '';
        do {
            $res = ($current % $this->system) . $res;
            $current = floor($current / $this->system);
        } while ($current > 0);

        return $res;
    }

    public function dvd($input)
    {
        return $input[0] * $input[2];
    }

    public function getString($string)
    {
        $match = preg_match('/^([0-9]*)(\*|\+|\-|\/)([0-9]*)/', $string, $result);
        if (!$match && !$result[2]) {
            return false;
        }
        array_shift($result);

        return $result;
    }

    public function makeItWork($input)
    {
        if ($input[1] && $input[1] === '+') {
            return $input[0] + $input[2];
        } elseif ($input[1] && $input[1] === '-') {
            return $input[0] - $input[2];
        } elseif ($input[1] && $input[1] === '*') {
            return $input[0] * $input[2];
        } elseif ($input[1] && $input[1] === '/') {
            return $input[0] / $input[2];
        }

        return null;
    }

    public function mltpl($input)
    {
        return $input[0] * $input[2];
    }

    public function pwer($string)
    {
        $match = preg_match('/^([0-9]*)(\^)([0-9]*)/', $string, $result);
        if (!$match && !$result[2]) {
            return false;
        }
        array_shift($result);

        return $result[0] ** $result[2];
    }

    public function sbstrct($input)
    {
        return $input[0] - $input[2];
    }

    public function bintodec($input)
    {
        $string = str_split((string)$input);
        $result = 0;
        for ($i = 0, $length = count($string); $i <= $length; $i++ ){
            $current = array_pop($string);
            $temp = $current * (2 ** $i);
            $result += $temp;
        }

        return $result;
    }
}