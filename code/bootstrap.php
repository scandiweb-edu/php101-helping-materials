<?php
//Autoloader lambda function
spl_autoload_register(function ($className) {
    $baseDir = __DIR__ . '/src';
    $class = str_replace('\\', '/', $className);
    include sprintf("%s/%s.php", $baseDir, $class);
});

//Bootstrap Resolver
$resolver = new \Tools\Resolver();

//Register interfaces and implementations
$resolver->register('Calculator', 'Calculator');